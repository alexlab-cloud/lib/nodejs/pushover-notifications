/**
 * A basic client for Pushover's push notification message API: https://pushover.net/api#messages.
 */

export const DEFAULT_PUSHOVER_API_URL = new URL('https://api.pushover.net/1/messages.json');

export interface PushoverConfig {
  token?: string;
  userKey?: string;
}

export interface PushoverNotificationOptions {
  title?: string;
  message?: string;
}

export enum PushoverResponseStatus {
  FAILURE = '0',
  SUCCESS = '1',
}

export interface PushoverMessageAPIResponse {
  request: string;
  status: PushoverResponseStatus;
  errors?: string[];
  token?: string;
  user?: string;
}

export interface PushoverNotificationResponse {
  title?: string;
  message?: string;
  requestId: string;
  status: PushoverResponseStatus;
}

export class PushoverClient {
  token: string;
  userKey: string;
  apiUrl: URL = DEFAULT_PUSHOVER_API_URL;

  constructor(config: PushoverConfig = {}) {
    if (!config.token) {
      throw new Error('A valid Pushover API token is required to initialize.');
    }

    this.token = config.token;

    if (!config.userKey) {
      throw new Error('A valid Pushover User Key is required to initialize.');
    }

    this.userKey = config.userKey;
  }

  public async notify(opts: PushoverNotificationOptions = {}): Promise<PushoverNotificationResponse> {
    const headers: Headers = new Headers();
    headers.set('Content-type', 'application/json');

    const notificationRequest = new Request(this.apiUrl, {
      method: 'POST',
      headers,
      body: JSON.stringify({ ...opts, token: this.token, user: this.userKey }),
    });

    return fetch(notificationRequest).then(async (res: Response) => {
      const data = (await res.json()) as PushoverMessageAPIResponse;

      if (data.errors) {
        if (data.token && data.token === 'invalid') {
          // If the `token` field exists on the response, it's likely due to an invalid API token
          throw new Error('Pushover API token invalid.');
        } else if (data.user && data.user === 'invalid') {
          // Similarly, if the `user` field exists on the response, it's likely due to an invalid User Key
          throw new Error('Pushover User Key invalid.');
        }
      }

      return { requestId: data.request, status: data.status, title: opts.title, message: opts.message };
    });
  }
}
