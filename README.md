# pushover-notifications

`@alexlab-cloud/pushover-notifications`

<img src="https://img.shields.io/badge/TypeScript-007ACC?style=for-the-badge&logo=typescript&logoColor=white" />

<img src="https://img.shields.io/badge/pnpm-%234a4a4a.svg?style=for-the-badge&logo=pnpm&logoColor=f69220" />

<img alt="node-current" src="https://img.shields.io/badge/node.js-6DA55F?style=for-the-badge&logo=node.js&logoColor=white">

<hr>


A simple client for [Pushover's](https://pushover.net) [push notification message API](https://pushover.net/api#messages).

## Installing

```bash
pnpm add @alexlab-cloud/pushover-notifications
```

## Using

```typescript
import { PushoverClient } from '@alexlab-cloud/pushover-notifications';

let pushover = new PushoverClient({ token: 'g8lhb9n99m32w88bb8xo5e71desmza', userKey: '9tbe81crmnb7gsghgxkrdlk4xdfl7u' });

pushover.notify({ title: 'This is a brand new test title', message: 'This is a brand new test message' }).then((res) => {
  // Log the client response to view information about the request to Pushover's API
  console.log(res);
});
```

<hr>

<sub>Emoji used for repository logo designed by <a href="https://openmoji.org/">OpenMoji</a> – the open-source emoji and icon project. License: CC BY-SA 4.0</sub>

<hr>
