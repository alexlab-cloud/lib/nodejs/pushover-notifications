#!/bin/bash

# Extension script containing shell commands that are run once the container has been started.

# shellcheck source=/dev/null
source /home/vscode/.bashrc

# Install Node packages
pnpm install
